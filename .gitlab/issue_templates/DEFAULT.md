Node Version: `X.Y.Z`

Node BACstack Version: `X.Y.Z`

- [ ] Bug Report
- [ ] Feature Request
- [ ] Question

**Note:** Make sure you have read the [FAQs](https://gitlab.com/fh1ch/node-bacstack/-/blob/master/FAQ.md)
before logging this issue.

### Feature Request / Question

-

### Current Behaviour (Bug Report)

-

### Expected Behaviour  (Bug Report)

-

### Steps to reproduce Issue  (Bug Report)

-
